Para poder probar el proyecto en postman, es necesario:

1. Mostrar datos: Seleccionar GET e ingresar la url de la tabla que desea ver.
	Tipo de empleados:  http://localhost:8080/empleados/findAllTipoEmpleado
	Empleados:	    http://localhost:8080/empleados/findAllEmpleado
   Luego presionar Send y en la pantalla se desplegará el contenido actual de la base de datos.

2. Guardar datos: Para guardar los datos es necesario seleccionar "Body" ubicado bajo la barra del url,
   seleccionar "raw" y luego al costado derecho donde dice por defecto "Text" cambiar por "Json".
   En el espacio disponible, debe ingresar un Json como el siguiente ejemplo:
   
   {
        "tipoEmpleadoId": 1,
        "nombre": "juan",
        "apellidos": "perez gonzalez",
        "telefono": 123456789
   }

   Luego en la parte de la url debe seleccionar POST e ingresar la siguiente url:
	http://localhost:8080/empleados/saveOrUpdate
   Y presionar "Send". Esto le entregará el mensaje "Guardado exitosamente".

3. Modificar datos: Para editar los datos ya ingresados, es necesario seleccionar "Body",
   seleccionar "raw" cambiar el botón "Text" por "Json".
   En el espacio disponible, ingresar el Json con el identificador y cambiar los valores que se deseen,
   como en el siguiente ejemplo:
   
   {
        "empleadoId": 7,
        "tipoEmpleadoId": 2,
        "nombre": "juan",
        "apellidos": "martinez Vejar",
        "telefono": 123456789
   }

   Luego en la url debe seleccionar POST e ingresar la siguiente url:
	http://localhost:8080/empleados/saveOrUpdate
   Y presionar "Send". Esto le entregará el mensaje "Guardado exitosamente".

4. Eliminar datos: Para eliminar deberá seleccionar "DELETE" e ingresar la url 
   http://localhost:8080/empleados/delete/(identificador), como muestra el siguiente ejemplo:
   http://localhost:8080/empleados/delete/4. Esto entregará el mensaje: "Eliminación exitosa".





Lo siguiente muestra el cómo están compuestas las tablas.

	TIPO EMPLEADO

create table tipo_empleado (
tipo_empleado_id serial not null,
nombre varchar(50),
descripcion varchar(100),
constraint pk_tipo_empleado primary key (tipo_empleado_id)
);


insert into tipo_empleado (nombre, descripcion) values ('Director', 'Se encarga de dirigir proyectos');
insert into tipo_empleado (nombre, descripcion) values ('Vendedor', 'Se encarga de ofrecer productos');


	EMPLEADO

create table empleado (
empleado_id serial not null,
tipo_empleado_id int4 not null,
nombre varchar(50),
apellidos varchar(50),
telefono numeric,
constraint pk_empleado primary key (empleado_id),
constraint fk_empleado_tipo_empleado foreign key (tipo_empleado_id) references tipo_empleado (tipo_empleado_id)
);


insert into empleado (tipo_empleado_id, nombre, apellidos, telefono) values (1, 'juan', 'sandoval riquelme', '123456789');
insert into empleado (tipo_empleado_id, nombre, apellidos, telefono) values (2, 'sofia', 'retamal silva', '987654321');





Las pruebas unitarias no fueron realizadas, ya que se me presentaron muchas dudas para su correcto desarrollo.


	