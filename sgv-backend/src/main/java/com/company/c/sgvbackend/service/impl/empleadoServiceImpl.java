package com.company.c.sgvbackend.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.company.c.sgvbackend.dao.tipoEmpleadoRepository;
import com.company.c.sgvbackend.dao.empleadoRepository;
import com.company.c.sgvbackend.domain.empleado;
import com.company.c.sgvbackend.domain.tipoEmpleado;
import com.company.c.sgvbackend.service.empleadoService;

@Service
@Transactional("transactionManager")
public class empleadoServiceImpl implements empleadoService{

	@Autowired
	private tipoEmpleadoRepository tipoEmpleadoRepository;
	
	@Autowired
	private empleadoRepository empleadoRepository;
	
	@Override
	public List<tipoEmpleado> findAllTipoEmpleado(){
		return tipoEmpleadoRepository.findAll();
	}

	@Override
	public List<empleado> findAllEmpleado() {
		return empleadoRepository.findAll();
		//return null;
	}

	@Override
	public void saveOrUpdate(empleado empleado) {
		// TODO Auto-generated method stub
		//int empleadoI = empleado.getEmpleadoId();
		
		if(empleado.getEmpleadoId() != null) {
			//update 
			empleado empleadoStored = empleadoRepository.getOne(empleado.getEmpleadoId());
			empleadoStored.setTipoEmpleadoId(empleado.getTipoEmpleadoId());
			empleadoStored.setNombre(empleado.getNombre());
			empleadoStored.setApellidos(empleado.getApellidos());
			empleadoStored.setTelefono(empleado.getTelefono());
		}else {
			//save
				empleadoRepository.save(empleado);				
		}
		
	}

	@Override
	public void delete(Long empleadoId) {
		// TODO Auto-generated method stub
		empleadoRepository.deleteById(empleadoId);
	}

	
}
