package com.company.c.sgvbackend.controller;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.company.c.sgvbackend.domain.empleado;
import com.company.c.sgvbackend.domain.tipoEmpleado;
import com.company.c.sgvbackend.facade.empleadoFacade;

@RestController
@RequestMapping("/empleados")
public class empleadoController {

	@Autowired
	private empleadoFacade empleadoFacade;
	
	@CrossOrigin()
	@GetMapping("findAllTipoEmpleado")
	public Map<String, Object> findAllTipoEmpleado(){
		Map<String, Object> map = new HashMap<String, Object>();
		List<tipoEmpleado> tipoEmpleados = empleadoFacade.findAllTipoEmpleado();
		map.put("data", tipoEmpleados);
		map.put("totalCount", tipoEmpleados.size());
		map.put("success", Boolean.TRUE);
		
		return map;
	}
	
	@CrossOrigin()
	@GetMapping("findAllEmpleado")
	public Map<String, Object> findAllEmpleado(){
		Map<String, Object> map = new HashMap<String, Object>();
		List<empleado> empleados = empleadoFacade.findAllEmpleado();
		map.put("data", empleados);
		map.put("totalCount", empleados.size());
		map.put("success", Boolean.TRUE);
		
		return map;
	}
	
	@CrossOrigin()
	@PostMapping("saveOrUpdate")
	public Map<String, Object> saveOrUpdate(@RequestBody empleado empleado){
		Map<String, Object> map = new HashMap<String, Object>();
		empleadoFacade.saveOrUpdate(empleado);
		map.put("message", "Guardado exitosamente");
		map.put("success", Boolean.TRUE);
		
		return map;
	}
	
	@CrossOrigin()
	@DeleteMapping("delete/{empleadoId}")
	public Map<String, Object> delete(@PathVariable Long empleadoId){
		Map<String, Object> map = new HashMap<String, Object>();
		empleadoFacade.delete(empleadoId);
		map.put("message", "Eliminación exitosa");
		map.put("success", Boolean.TRUE);
		
		return map;
	}
}
