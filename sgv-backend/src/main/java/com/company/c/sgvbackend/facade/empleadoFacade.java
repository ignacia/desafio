package com.company.c.sgvbackend.facade;

import java.util.List;

import com.company.c.sgvbackend.domain.empleado;
import com.company.c.sgvbackend.domain.tipoEmpleado;

public interface empleadoFacade {

	List<tipoEmpleado> findAllTipoEmpleado();
	List<empleado> findAllEmpleado();
	void saveOrUpdate(empleado empleado);
	void delete(Long empleadoId);
}
