package com.company.c.sgvbackend.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.company.c.sgvbackend.domain.tipoEmpleado;

@Repository
public interface tipoEmpleadoRepository extends JpaRepository<tipoEmpleado, Long>{

}
