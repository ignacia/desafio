package com.company.c.sgvbackend.service;

import java.util.*;

import com.company.c.sgvbackend.domain.empleado;
import com.company.c.sgvbackend.domain.tipoEmpleado;

public interface empleadoService {

	List<tipoEmpleado> findAllTipoEmpleado();
	List<empleado> findAllEmpleado();
	void saveOrUpdate(empleado empleado);
	void delete(Long empleadoId);
}