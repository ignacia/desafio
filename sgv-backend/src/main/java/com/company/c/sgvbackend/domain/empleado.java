package com.company.c.sgvbackend.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name ="empleado", schema = "public")
public class empleado implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3778637970539370708L;
	@Id
	@GeneratedValue(generator = "empleadoSeq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "empleadoSeq", sequenceName = "empleado_seq", allocationSize = 1, initialValue = 1)
	
	@Column(name = "empleado_id", unique = true, nullable = false)
	private Long empleadoId;
	
	@Column(name = "tipo_empleado_id")
	private Long tipoEmpleadoId;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "apellidos")
	private String apellidos;
	
	@Column(name = "telefono")
	private int telefono;
	
	public empleado() {
		
	}

	public Long getEmpleadoId() {
		return empleadoId;
	}

	public void setEmpleadoId(Long empleadoId) {
		this.empleadoId = empleadoId;
	}

	public long getTipoEmpleadoId() {
		return tipoEmpleadoId;
	}

	public void setTipoEmpleadoId(Long tipoEmpleadoId) {
		this.tipoEmpleadoId = tipoEmpleadoId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}
	
	
}
