package com.company.c.sgvbackend.facade.impl;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.company.c.sgvbackend.domain.empleado;
import com.company.c.sgvbackend.domain.tipoEmpleado;
import com.company.c.sgvbackend.facade.empleadoFacade;
import com.company.c.sgvbackend.service.empleadoService;

@Component("empleadoFacade")
public class empleadoFacadeImpl implements empleadoFacade{

	@Autowired
	private empleadoService empleadoService;
	
	@Override
	public List<tipoEmpleado> findAllTipoEmpleado() {
		return empleadoService.findAllTipoEmpleado();
	}

	@Override
	public List<empleado> findAllEmpleado() {
		return empleadoService.findAllEmpleado();
		//return null;
	}

	@Override
	public void saveOrUpdate(empleado empleado) {
		empleadoService.saveOrUpdate(empleado);
		
	}

	@Override
	public void delete(Long empleadoId) {
		// TODO Auto-generated method stub
		empleadoService.delete(empleadoId);
		
	}
	
	
	
}
