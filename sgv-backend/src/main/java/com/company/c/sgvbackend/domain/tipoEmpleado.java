package com.company.c.sgvbackend.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "tipoEmpleado", schema = "public")
public class tipoEmpleado implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6476324973149532717L;
	@Id
	@GeneratedValue(generator = "tipoEmpleadoSeq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "tipoEmpleadoSeq", sequenceName = "tipo_empleado_seq", allocationSize = 1, initialValue = 1)
	
	@Column(name = "tipo_empleado_id", unique = true, nullable = false)
	private Long tipoEmpleadoId;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "descripcion")
	private String descripcion;
	
	public tipoEmpleado() {
		
	}

	public Long getTipoEmpleadoId() {
		return tipoEmpleadoId;
	}

	public void setTipoEmpleadoId(Long tipoEmpleadoId) {
		this.tipoEmpleadoId = tipoEmpleadoId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
